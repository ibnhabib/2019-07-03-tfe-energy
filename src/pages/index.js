import React from "react";
import { Waypoint } from 'react-waypoint';
import { scroller } from 'react-scroll';
import { Controller, Scene } from 'react-scrollmagic/dist/index';
import Layout from "../components/layout";
import SEO from "../components/seo";
import styled from "styled-components";
import Logo  from '../components/logo';
import VideoPlayer from '../components/videoPlayer';
import Navbar from '../components/navbar';
import MobileNavbar from '../components/mobileNavbar';
import ContactPage from '../components/contact';
import Footer from '../components/footer';
import TextPartComponent from '../components/textPart';
import VIDEOS from '../constants/videos'
import CarouselComponent from '../components/pureCarousel';
import GridComponent from '../components/grid';

import 'semantic-ui-css/components/reset.css';
import 'semantic-ui-css/components/label.css';
import 'semantic-ui-css/components/icon.css';
import 'semantic-ui-css/components/divider.css';
import "./index.scss";
import '../styles/animations.scss';
import '../styles/navbar.scss';

import { graphql } from "gatsby"

class IndexPage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
        videosLoaded: 0,
        videosEmbedded: VIDEOS.length,
        bigLogo: true,
    };
    this.videoIsReady = this.videoIsReady.bind(this);
    this.scrollTo = this.scrollTo.bind(this);
    this.setElClass = this.setElClass.bind(this);
  }
  componentDidMount () {
  	console.log(this.props.data.allContentfulProject.edges);
  }
	setElClass (el, classList) {
    document.querySelector(el).classList = classList
  }
  videoIsReady () {
    console.log('Child said video is ready.')
	  if (this.state.videosLoaded === this.state.videosEmbedded) {
	  	return;
	  }
    this.setState({
      videosLoaded: this.state.videosLoaded + 1
    })
  }
	scrollTo (element) {
  	console.log(element);
		scroller.scrollTo(element, {
			duration: 800,
			delay: 0,
			smooth: 'easeInOutQuart'
		})
	};
  render () {
	  const textParts = this.props.data.allContentfulTextParts.edges.sort((a,b) => parseInt(a.node.order) - parseInt(b.node.order));
    const BreakSection = styled.div`
			padding: 1rem;
      max-width: 1024px;
      padding-left: 2rem;
      padding-right: 2rem;
      @media screen and (max-width: 599px) {
      	padding-left: 1rem;
      	padding-right: 1rem;
      }
      margin: 0 auto;
      z-index: 99;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      min-height: 50vh;
      // background: black;
      color: white;
    	text-shadow: -1px -1px 1px rgba(255,255,255,.1), 1px 1px 1px rgba(0,0,0,.5);
      text-align: left;
      h1 {
      	text-align: left;
      	width: 100%;
      	@media screen and (max-width: 599px) {
      		margin-top: 2.5rem;
      	}
      }
      p {
        text-align: left;
        font-size: 150%;
        @media screen and (max-width: 599px) {
        	font-size: 110%;
        }
        line-height: 1.2;
      }
      .auto-height {
        margin: 1rem auto;
        min-height: unset;
      }
      &.sixth-height {
      	min-height: 15vh;
      }
      &.fifth-height {
      	min-height: 20vh;
      }
      &.quarter-height {
        min-height: 25vh;
      }
      &.half-height {
        min-height: 50vh;
      }
      &.full-height {
        min-height: 100vh;
      }
      &.carousel {
        // max-width: 90%;
        // margin: auto;
        min-height: unset;
        height: auto;
        margin-bottom: 10vh;
        display: block;
      	flex-direction: unset;
      	align-items: unset;
	      @media screen and (max-width: 599px) {
		        // padding-bottom: 5vh;
						min-height: calc(50vh + 10vh);
						margin: 0;
						padding: 10px;
					
					}
      	}
      &.contact {
      	max-width: 100%;
      	h1 {
      		text-align: center;
      	}
      }
    `
	  const TextPartSettings = {
    	heightClass: 'sixth-height',
		  duration: '45%',
		  triggerHook: 0.3,
	  }

	  const projectAndReportData = this.props.data.allContentfulProject.edges
	  const hasAlerts = this.props.data.allContentfulAlert.edges.length > 0
	  const activeAlerts = this.props.data.allContentfulAlert.edges

	  console.log(activeAlerts)

    return (
      <Layout>
	      <a href='/' style={{display: 'none'}} id={'home'}>Home</a>
        <SEO title="Home" keywords={[`tfe`, `energy`, `welcome`]} />
        <Navbar alerts={hasAlerts ? activeAlerts : ''} />
        <MobileNavbar
	        alerts={hasAlerts ? activeAlerts : ''}
        />
        <Controller>
	        <Scene pin={true} triggerHook={0}>
		        <div className="section">
			        <VideoPlayer
				        id={'video1'}
				        loadingHandler={this.videoIsReady}
				        video={VIDEOS[0]}
			        />
		        </div>
	        </Scene>
          <Scene
	          pin={false}
	          duration={'50%'}
	          triggerHook={0}
          >
            <div className="section">
	            <div>
		            <Waypoint
			            topOffset={'50%'}
			            onEnter={() => {
			            	this.setElClass('img#tfe-logo','big');
				            this.setElClass('div#scroll-arrow','visible');
			            }}
			            onLeave={() => {
			            	this.setElClass('img#tfe-logo','nav');
				            this.setElClass('#scroll-arrow','hidden');
			            }}

		            />
		            <Logo />
	            </div>
            </div>
          </Scene>
          <Scene
	          pin={true}
	          duration={TextPartSettings.duration}
	          triggerHook={0.25}
          >
            <div>
              <BreakSection className={'half-height'}>
                <Waypoint onEnter={() => console.log('entered section 1')} />
                <TextPartComponent data={textParts[0].node} />
	              {/*<p>We believe in the future, because we believe that the required change is within our grasp, if we can refocus minds, innovate and invest accordingly.</p>*/}
              </BreakSection>
            </div>
          </Scene>
	        <Scene pin={true} duration={TextPartSettings.duration} triggerHook={TextPartSettings.triggerHook}>
		        <div className={'gradient-animation'}>
			        <BreakSection className={TextPartSettings.heightClass}>
                <Waypoint onEnter={() => console.log('entered section 1')} />
				        {/* TFE Energy */}
				        <TextPartComponent data={textParts[1].node} />
              </BreakSection>
            </div>
          </Scene>
	        <Scene triggerHook={0.25} pin={true} duration={'25%'}>
		        <div>
			        <BreakSection className={'auto-height'} id="projects" >
				        <Waypoint onEnter={() => console.log('entered project section')} />
				        {/*Our work*/}
				        <TextPartComponent data={textParts[2].node} />
			        </BreakSection>
		        </div>
	        </Scene>
	        <Scene triggerHook={0} pin={false}>
		        <div>
			        <BreakSection
				        className={'half-height carousel'}
			        >
				        <Waypoint onEnter={() => console.log('entered project carousel')} />
				        <CarouselComponent
					        linkToAll={'/our-work/'}
					        linkToSingle={'/project/'}
					        title={'Project gallery'}
					        data={projectAndReportData}
				        />
			        </BreakSection>
		        </div>
	        </Scene>
          <Scene pin={true} triggerHook={0}>
            <div className="section">
              <VideoPlayer
                loadingHandler={() => this.videoIsReady()}
                video={VIDEOS[1]}
              />
            </div>
          </Scene>
	        <Scene pin={true} duration={TextPartSettings.duration} triggerHook={0.25}>
		        <div className={'gradient-animation'}>
			        <BreakSection className={'half-height'}>
				        <Waypoint onEnter={() => console.log('entered section 1 of video 2')} />
				        {/*We, at TFE Energy are optimists*/}
				        <TextPartComponent data={textParts[3].node} />
			        </BreakSection>
		        </div>
	        </Scene>
	        {/*
	        <Scene pin={true} duration={TextPartSettings.duration} triggerHook={TextPartSettings.triggerHook}>
		        <div>
			        <BreakSection className={TextPartSettings.heightClass}>
				        <Waypoint onEnter={() => console.log('entered section 3 of video 2')} />
				        <p>Capturing the opportunity to limit climate change requires fast, smart and decisive action. Most importantly, it requires a deep conviction that it can be done.</p>
			        </BreakSection>
		        </div>
	        </Scene>
	        */}
	        <Scene triggerHook={0.25} pin={true} duration={'25%'}>
		        <div>
			        <BreakSection id="reports" className={'auto-height'}>
				        <Waypoint onEnter={() => console.log('entered reports section')} />
				        {/*}We work with*/}
				        <TextPartComponent data={textParts[4].node} />
			          <GridComponent
				          styles={{
				          	marginTop: '2rem'
				          }}
				          columnBackground={'white'}
				          stackable
									data={this.props.data.allContentfulPartners.edges}
			          />
			        </BreakSection>
		        </div>
	        </Scene>
	        <Scene triggerHook={0} pin={false}>
		        <div>
			        <BreakSection className={'full-height carousel'}>
				        <Waypoint onEnter={() => console.log('entered reports carousel')} />
			        </BreakSection>
		        </div>
	        </Scene>
          <Scene pin={true} triggerHook={0}>
            <div className="section">
              <VideoPlayer
                loadingHandler={() => this.videoIsReady()}
                video={VIDEOS[2]}
              />
            </div>
          </Scene>
	        {/*
	        <Scene pin={true} duration={TextPartSettings.duration} triggerHook={TextPartSettings.triggerHook}>
		        <div>
			        <BreakSection className={TextPartSettings.heightClass}>
				        <Waypoint onEnter={() => console.log('entered section 1 of video 3')}/>
	              <TextPartComponent data={textParts[5].node}/>
              </BreakSection>
            </div>
	        </Scene>
	        */}
	        <Scene duration={'5%'} triggerHook={0} pin={true}>
			      <BreakSection id={"contact"} className={'full-height gradient-animation contact'}>
			        <Waypoint onEnter={() => console.log('entered contact')} />
			        <ContactPage />
		        </BreakSection>
		      </Scene>
	        <Scene pin={true} triggerHook={0}>
		        <Footer />
	        </Scene>
        </Controller>
      </Layout>
    )
  }
}

export default IndexPage;

export const query = graphql`
  query {
    allCloudinaryMedia {
      edges {
        node {
          id
          type
          url
        }
      }
    }
    allContentfulAlert(sort: {order: DESC, fields: updatedAt}) {
        edges {
            node {
                id
                text
                linkText
		            link
                color
                updatedAt
            }
        }
    }
	  allContentfulPartners {
	    edges {
	      node {
	        id
	        title
	        description
	        image {
	          file {
	            url
	          }
	        }
	        url
	      }
	    }
	  }
	  allContentfulTextParts(
			sort: {
					fields: [order]
					order: ASC
			}	  
	  ) {
	      edges {
	          node {
	              id
			          order
	              text {
	                  content {
	                      content {
	                          value
	                      }
	                  }
	              }
	              title {
	                  title
	              }
	          }
	      }
	  }
	  allContentfulProject {
	    edges {
	      node {
	        id
	        type
	        slug
	        title
	        updatedAt
	        image {
	          file {
	            url
	            fileName
	            contentType
	            details {
	              image {
	                width
	                height
	              }
	            }
	          }
	          description
	        }
	        contentful_id
	        body {
	          body
	        }
	        tags
	        description
	      }
	    }
	  }
  }
`;
