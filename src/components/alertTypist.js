import React from "react";
import styled from "styled-components";
import Typist from 'react-typist';
import renderHTML from 'react-render-html';
import 'react-typist/dist/Typist.css';

const InvisibleMessage = styled.span`
	display: none;
	opacity: 0;
	@media screen and (max-width: 500px) {
		display: block;
		position: relative;
		float: right;
		right: 1rem;
		max-width: 85%;
		opacity: 0;
		margin: 0;
		text-align: right;
		padding: 0.25rem .75rem 0.75rem .5rem;
	}
`

const TypistWrapper = styled.div`
	width: 100%;
	@media screen and (max-width: 500px) {
		position: absolute;
		top: 0;
		right: 0;
		.alert-typist {
			padding-top: .5rem;
		}
	}
`

class TypistComponent extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
			isFinished: false
		}
	}
	render() {
		const { textPieces } = this.props
		const { delay } = this.props
		const TypistContainer = styled.div`
			color: white;
		`
		return (
			<TypistContainer>
					<InvisibleMessage>{renderHTML(textPieces.text)}&bsp;{renderHTML(textPieces.readMore)}</InvisibleMessage>
					<TypistWrapper>
						<Typist
							speed={5}
							blinkSpeed={5}
							cursor={{
								blink: true,
								element: '|'
							}}
							onTypingDone={() => this.props.updateHandler()}
							startDelay={0}
							className={'alert-typist'}
						>
								<span
									key={`text-part-${textPieces.id}`}
								>
								<span
									className={'typist-header'}
									style={{
										borderBottom: '2px solid transparent'
									}}
								>
									{ renderHTML(textPieces.text)  }&nbsp;
								</span>
								<Typist.Delay
									ms={ delay }
								/>
								<span
									className={'typist-header'}
									style={{
										borderBottom: '2px solid white'
									}}
								>
									{ renderHTML(textPieces.readMore) }
								</span>
							</span>
						</Typist>
					</TypistWrapper>
			</TypistContainer>
		);
	}
}
export default TypistComponent;
