import React from "react";
import styled from "styled-components";
import Typist from 'react-typist';
import renderHTML from 'react-render-html';
import 'react-typist/dist/Typist.css';

class TypistComponent extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
			isFinished: false
		}
	}
	render() {
		const textPieces = [
			{ text: 'We are ',
				html: 'We are ',
			back: 0},
			{ text: ' African',
				html: ' African'},
			{ text: ' European',
				html: ' European'},
			{ text: ' optimists',
				html: ' optimists'},
			{ text: ' realists',
				html: ' realists'},
			{ text: ' entrepreneurs',
				html: ' entrepreneurs'},
			{ text: ' believers',
				html: ' believers'},
			{ text: ' parents',
				html: ' parents'},
			{ text: ' children',
				html: ' children',
			},
			{ text: ' thinkers',
				html: ' Thinkers',
			},
			{ text: ' doers',
				html: ' doers',
				back: 0},
		]
		const { startDelay, delay } = this.props
		const mappedTexts = textPieces.map((text, i) => (
			<span key={`text-part-${i}`}>
				<span className={'typist-header'}>
					{ renderHTML(text.html) }
				</span>
				<Typist.Backspace
					count={ text.back || text.back === 0 ? text.back : text.text.length }
					delay={delay * 3}
				/>
				<Typist.Delay
					ms={ delay }
				/>
			</span>
		))
		const TypistContainer = styled.div`
			color: white;
			display: flex;
			margin-top: 2rem;
			span {
				font-family: 'Orbitron', sans-serif;
				font-size: 2rem;
				@media screen and (max-width: 500px) {
					font-size: 1.5rem;
				}
			}
		`
		return (
			<TypistContainer style={{display: 'none'}}>
				<Typist
					speed={2}
					blinkSpeed={5}
					className={'typist-header'}
					cursor={{
						blink: true,
						element: '|'
					}}
					startDelay={startDelay}
					onTypingDone={() => this.setState({isFinished: true})}
				>
					{ mappedTexts }
				</Typist>
			</TypistContainer>
		);
	}
}
export default TypistComponent;
