import React from "react"
import styled from 'styled-components';
import '../styles/animations.scss';
import TypistComponent from '../components/alertTypist'
import 'semantic-ui-css/components/icon.css';
import 'semantic-ui-css/components/button.css';

const AlertContainer = styled.a`
	text-decoration: none;
	z-index: 999;
	color: white;
	position: relative;
	display: flex;
	top: 0;
	left: 0;
	width: 100%;
	min-height: 3.2rem;
	text-align: center;
	div {
		width: 100%;
		display: inline;
		margin: 0 auto;
		font-size: 1.2rem;
		padding: 1rem;
		@media screen and (max-width: 500px) {
			.alert-typist {
				width: 100%;
				min-height: 36px;
				vertical-align: bottom;
			}
			padding: .25rem .75rem 0 .5rem;
			margin: 0 !important;
			font-size: 0.9rem;
			line-height: 1.25;
			border-bottom: 2px solid transparent;
		}
	}
`

class AlertMessage extends React.Component {
	constructor (props) {
		super(props);
		const alertArray = Object.keys(this.props.data).map((key,i) => (
			{
				text: this.props.data[key].node.text,
				readMore: this.props.data[key].node.linkText,
				link: this.props.data[key].node.link,
				id: this.props.data[key].node.id,
				back: parseInt(this.props.data[key].node.length + this.props.data[key].node.text.length)
			}
		))
		this.state = {
			currentAlert: alertArray[0],
			currentIndex: 0,
			alertArray: alertArray
		}
		this.updateHandler = this.updateHandler.bind(this)
	}
	updateHandler = () => {
		const { currentIndex, alertArray } = this.state
		if (this.state.alertArray[currentIndex + 1]) {
			setTimeout(() => {
				this.setState({
					currentIndex: currentIndex + 1,
					currentAlert: alertArray[currentIndex + 1]
				})
			}, 5000)
		} else {
			if (alertArray.length > 1) {
				setTimeout(() => {
					this.setState({
						currentIndex: 0,
						currentAlert: alertArray[0]
					})
				}, 5000)
			}
		}
	}

	render () {
		return (
			<AlertContainer
				className={'gradient-animation'}
				href={this.state.currentAlert.link}
				target={'_blank'}
				rel={'noopener noreferrer'}
			>
				<TypistComponent
					startDelay={2}
					delay={3}
					textPieces={this.state.currentAlert}
					updateHandler={this.updateHandler}
				/>
			</AlertContainer>
		)
	}
}

export default AlertMessage
