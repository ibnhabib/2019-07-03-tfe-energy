import React from 'react';

export default function TextPartComponent({data}) {
	const paragraphs = data.text.content.map((el,i) => (
		<p key={`textpart-${i}`}>{el.content[0].value}</p>
	));
	return (
		<div style={{textAlign: 'left', width: '100%'}}>
			{data.title && <h1>{data.title.title}</h1> }
			{paragraphs ? paragraphs : ''}
		</div>
	);
}
