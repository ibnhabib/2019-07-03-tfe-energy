import React from "react";
import { Link, graphql } from "gatsby";
import {Helmet} from "react-helmet";
import { BLOCKS, MARKS, INLINES } from "@contentful/rich-text-types"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"
import Layout from "../components/layout";
import SEO from "../components/seo";
import styled from "styled-components";
import 'semantic-ui-css/components/list.css';
import 'semantic-ui-css/components/item.css';
import 'semantic-ui-css/components/icon.css';
const Project = ({ data }) => {
	const { title, image, tags, description, files, slug, type} = data.contentfulProject;

	const TagLabel = styled.div``

	const StyledLink = styled.a`
		color: white;
	`

	const content = data.contentfulProject;

	const options = {
		renderMark: {
			[INLINES.HYPERLINK]: link => <a target={link.target} rel={'noopener noreferrer'} href={link.href}>{content}</a>,
			[MARKS.BOLD]: text => <strong>{text}</strong>,
			[BLOCKS.HEADING_1]: text => <h1>{text}</h1>,
			[BLOCKS.HEADING_2]: text => <h2>{text}</h2>,
			[BLOCKS.HEADING_3]: text => <h3>{text}</h3>,
			[BLOCKS.HEADING_4]: text => <h4>{text}</h4>,
			[BLOCKS.HEADING_5]: text => <h5>{text}</h5>,
			[BLOCKS.HEADING_6]: text => <h6>{text}</h6>,
			[BLOCKS.LIST_ITEM]: text => <h4>{text}</h4>,
			[BLOCKS.UL_LIST]: text => <h4>{text}</h4>,
			[BLOCKS.OL_LIST]: text => <ol>{text}</ol>,
			[BLOCKS.PARAGRAPH]: text => <p>{text}</p>,
		},
		renderNode: {
			[BLOCKS.PARAGRAPH]: (node, children) => <p>{children}</p>,
		},
	}

	const StyledDiv = styled.div`
	.project-body {
	    line-height: 1.3rem;
	    font-size: 1rem;
	    a, a:visited, a:active {
	        color: white;
	        text-decoration: none;
	        border-bottom: 2px solid white;
	    }
	    a:hover {
	      background: white;
	      color: black;
      }
	}
	min-height: 100vh;
	overflow: hidden;
	margin-bottom: 5rem;
	a.button:not(.icon) {
	    font-family: 'Roboto',sans-serif;
	}
	h1 {
	    display: inline;
	    font-weight: normal;
	    font-size: 1.5rem;
	    @media screen and (max-width:500px) {
	        font-size: 1.4rem;
	    }
	}
	h2 {
	    font-size: 1.5rem;
	}
	.project-description {
	    font-family: 'Roboto',sans-serif;
	    color: white;
	    font-size: 1.4rem;
	    line-height: 1.6rem;
	}
	.project-tags {
	    button {
	        margin-bottom: .5rem;
	    }
	}
	.project-downloads {
	    .downloads-list {
	        *:not(.icon) {
	            font-family: 'Roboto',sans-serif;
	            color: white !important;
	        }
	        a, a:visited, a:hover, a:active {
	            font-family: 'Roboto',sans-serif;
	            text-decoration: none;
	            color: white;
	            border-bottom: 2px solid transparent;
	        }
	        a:hover {
	            border-bottom: 2px solid white;
	        }
	    }
	}
	.project-image {
	    width: 100%;
	}
  `
	const linkStyle = {
		color: 'white',
		textDecoration: 'none',
	}

	return (
		<Layout>
			<Helmet>
				<meta name="twitter:card" content="summary_large_image" />
				<meta name="twitter:site" content="@TFEdigital" />
				<meta name="twitter:title" content={title} />
				<meta name="twitter:description" content={description} />
				<meta property="twitter:image" content={`https:${image.file.url}`} />
				<meta property="og:image" content={`http:${image.file.url}`} />
				<meta property="og:image:secure_url" content={`https:${image.file.url}`} />
				<meta name="og:description" content={description} />
				<meta name="og:url" content={`https://www.tfe.energy/project/${slug}`} />
				<meta name="og:image:width" content={image.file.details.image.width} />
				<meta name="og:image:height" content={image.file.details.image.height} />
			</Helmet>
			<SEO title={title} />
			<StyledDiv className={'ui text container'}>
				{/* BACK BUTTON */}
				<div className={'ui vertical aligned segment'}>
					<Link to={`/`}
					      style={linkStyle}
					>
						<button className={'ui icon button'}>
							<i className={'icon chevron left'} /> Home
						</button>
					</Link>
					<Link to={`/our-work/`}
					      style={linkStyle}
					>
						<button className={'ui basic inverted icon button'}>
							<i className={'icon list'} /> Our work
						</button>
					</Link>
				</div>
				{/* IMAGE TITLE DESCRIPTION */}
				<div className="ui vertical aligned segment project-content">
					<img width={'100%'} className={'project-image'} alt={title} src={image.file.url} />
					<h1 className={'gradient-animation project-title'}>
						{title}
					</h1>
					<h2 className={'project-description'}>{description}</h2>
					<p className={'project-body'}>{documentToReactComponents(content.bodyRichtext.json, options)}</p>
				</div>
				{/* FILES */ }
				{files &&
				<div className={'ui very basic vertical aligned segment project-downloads'}>
					<h3>Downloads</h3>
					<div className="ui divided inverted items downloads-list">
						{files.map((file, i) => (
							<div className="item" key={`file-${i}`}>
								<div className={'content'}>
									<a className="header download-link"
									   href={file.file.url}
									   target={'_blank'}
									   rel="noopener noreferrer"
									>
										{file.title}<br />
									</a>
									<div className={'meta'}>
										{file.file.fileName} (File type: {file.file.contentType.split('/')[1]})
									</div>
								</div>
							</div>
						))}
					</div>
				</div>
				}
				<div className={'ui very basic vertical aligned segment project-tags'}>
					{type ? (
						<h3>Share this {type}</h3>
					) : (
						<h3>Share this</h3>
					)}
					<a href={`https://twitter.com/intent/tweet?text=https://www.tfe.energy/project/${slug}`}
					   style={linkStyle}
					   target={'_blank'}
					   rel="noopener noreferrer"
					>
						<TagLabel className="ui blue icon inverted tiny button">
							<i className={'icon twitter'} /> Share on Twitter
						</TagLabel>
					</a>
					<a href={`https://www.facebook.com/sharer/sharer.php?u=https://www.tfe.energy/project/${slug}`}
					   style={linkStyle}
					   target={'_blank'}
					   rel="noopener noreferrer"
					>
						<TagLabel className="ui blue icon inverted tiny button">
							<i className={'icon facebook'} /> Share on Facebook
						</TagLabel>
					</a>
					<a href={`https://www.linkedin.com/shareArticle?mini=true&url=https://www.tfe.energy/project/${slug}&image=${image.file.url}&title=${title}`}
					   style={linkStyle}
					   target={'_blank'}
					   rel="noopener noreferrer"
					>
						<TagLabel className="ui blue inverted tiny icon button">
							<i className={'icon linkedin'} /> Share on LinkedIn
						</TagLabel>
					</a>
				</div>
				{/* TAGS */ }
				{tags &&
				<div className={'ui very basic vertical aligned segment project-tags'} style={{display: 'none'}}>
					<h3>{type} tags</h3>
					{tags.map(tag => (
						<TagLabel className="ui basic inverted tiny button" style={{pointerEvents: 'none'}} key={tag}>
							{tag}
						</TagLabel>
					))}
				</div>
				}
			</StyledDiv>
		</Layout>
	);
};
export default Project;
export const query = graphql`
  query($slug: String!) {
    contentfulProject(slug: { eq: $slug }) {
      title
      slug
      description
	    bodyRichtext {
			    json
	    }
      files {
        id
          file {
            url
            fileName
            contentType
          }
        title
        description
      }
	  image {
          file {
            details {
              image {
                height
                width
              }
            }
		  url
	    }
      }
      tags
      type
    }
  }
`;
