const VIDEOS = [
	{
		pageFlow: 0,
		youtubeId: 'l0Xzw6uwNZ4',
		src: {
			poster: {
				jpg: 'https://res.cloudinary.com/marc-fehr-media/image/upload/c_scale,e_pixelate:5,q_auto:eco,w_1920/v1563790016/tfe/still1_w5rfil.jpg'
			},
			fullhd: {
				mp4: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653155/tfe/UV_DANCE_FINAL_for7n9.mp4',
				},
				webm: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653155/tfe/UV_DANCE_FINAL_for7n9.webm',
				},
				ogv: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653155/tfe/UV_DANCE_FINAL_for7n9.ogv',
				}
			},
			hdready: {
				mp4: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,w_480/v1563653155/tfe/UV_DANCE_FINAL_for7n9.mp4',
				},
				webm: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,w_480/v1563653155/tfe/UV_DANCE_FINAL_for7n9.webm',
				},
				ogv: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,w_480/v1563653155/tfe/UV_DANCE_FINAL_for7n9.ogv',
				}
			}
		},
		id: 0,
		title: 'test 2 low, web',
	},
	{
		pageFlow: 1,
		youtubeId: 'brHbeQp5BYk',
		src: {
			poster: {
				jpg: 'https://res.cloudinary.com/marc-fehr-media/image/upload/e_pixelate:10,q_auto:eco/v1563790006/tfe/still2_cllsfi.jpg'
			},
			fullhd: {
				mp4: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653153/tfe/FACE_PROJECTION_FINAL_kj5y92.mp4',
				},
				webm: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653153/tfe/FACE_PROJECTION_FINAL_kj5y92.webm',
				},
				ogv: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653153/tfe/FACE_PROJECTION_FINAL_kj5y92.ogv',
				}
			},
			hdready: {
				mp4: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,w_480/v1563653153/tfe/FACE_PROJECTION_FINAL_kj5y92.mp4',
				},
				webm: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,w_480/v1563653153/tfe/FACE_PROJECTION_FINAL_kj5y92.webm',
				},
				ogv: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,w_480/v1563653153/tfe/FACE_PROJECTION_FINAL_kj5y92.ogv',
				}
			}
		},
		id: 1,
		title: 'test 3 low, web',
	},
	{
		pageFlow: 2,
		youtubeId: 'D25X9hUe0uo',
		src: {
			poster: {
				jpg: 'https://res.cloudinary.com/marc-fehr-media/image/upload/e_pixelate:10,q_auto:eco/v1563790014/tfe/still3_sba0dt.jpg'
			},
			fullhd: {
				mp4: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,w_480/v1563653166/tfe/PURITY_BLINK_FINAL_xmiffv.mp4',
				},
				webm: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653166/tfe/PURITY_BLINK_FINAL_xmiffv.webm',
				},
				ogv: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:eco,w_1920/v1563653166/tfe/PURITY_BLINK_FINAL_xmiffv.ogv',
				}
			},
			hdready: {
				mp4: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,vc_auto,w_1280/v1563653166/tfe/PURITY_BLINK_FINAL_xmiffv.mp4',
				},
				webm: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,vc_auto,w_1280/v1563653166/tfe/PURITY_BLINK_FINAL_xmiffv.webm',
				},
				ogv: {
					cdn: 'https://res.cloudinary.com/marc-fehr-media/video/upload/ac_none,c_scale,q_auto:low,vc_auto,w_1280/v1563653166/tfe/PURITY_BLINK_FINAL_xmiffv.ogv',
				}
			}
		},
		id: 2,
		title: 'test 4 low, web'
	}
];

export default VIDEOS
