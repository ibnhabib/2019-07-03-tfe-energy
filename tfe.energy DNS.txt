; Domain: tfe.energy
; Exported (y-m-d hh:mm:ss): 2019-10-07 00:12:56
;
; This file is intended for use for informational and archival
; purposes ONLY and MUST be edited before use on a production
; DNS server.
;
; In particular, you must update the SOA record with the correct
; authoritative name server and contact e-mail address information,
; and add the correct NS records for the name servers which will
; be authoritative for this domain.
;
; For further information, please consult the BIND documentation
; located on the following website:
;
; http://www.isc.org/
;
; And RFC 1035:
;
; http://www.ietf.org/rfc/rfc1035.txt
;
; Please note that we do NOT offer technical support for any use
; of this zone data, the BIND name server, or any other third-
; party DNS software.
;
; Use at your own risk.


; SOA Record
tfe.energy.	3600	 IN 	SOA	ns75.domaincontrol.com.	dns.jomax.net. (
					2019100701
					28800
					7200
					604800
					3600
					) 

; A Records
@	600	 IN 	A	Parked

; CNAME Records
184.168.131.241	3600	 IN 	CNAME	prototype.tfe.energy.
www	3600	 IN 	CNAME	@
_domainconnect	3600	 IN 	CNAME	_domainconnect.gd.domaincontrol.com.

; MX Records
@	604800	 IN 	MX	10	aspmx.l.google.com.
@	604800	 IN 	MX	20	alt1.aspmx.l.google.com.
@	604800	 IN 	MX	30	alt2.aspmx.l.google.com.
@	604800	 IN 	MX	40	aspmx2.googlemail.com.
@	604800	 IN 	MX	50	aspmx3.googlemail.com.

; TXT Records
@	3600	 IN 	TXT	"google-site-verification=csLSDxBgGnPbOWDeWT3CJsFLJZ0vrmbsTkennyOc0oE"

; SRV Records

; AAAA Records

; CAA Records

; NS Records
@	3600	 IN 	NS	ns75.domaincontrol.com.
@	3600	 IN 	NS	ns76.domaincontrol.com.

